library(lubridate)
library(dplyr)

rw <- read.csv("../TrainingFiles/TrainingData.csv")
rw<-filter(rw,plan_code=='PERM')
rw$rptd_date<-as.POSIXct(rw$rptd_date)
rw$entry_dt<-NULL
rw$sum_entrydate<-as.POSIXct(rw$sum_entrydate)
rw$final_date<-as.POSIXct(rw$final_date)

number_current_apps<-function(entry,final){
  ct<-rep(0,length(entry))
  for (j in 1:(-1+length(entry))){
    for(i in (j+1):length(entry)){
      if(final[j]>entry[i]){
        ct[c(i,j)]=ct[c(i,j)]+1}
    }
  }
ct  
}

number_current_policies<-function(entry,final){
  ct<-rep(0,length(entry))
  for (j in 1:length(entry)){
     ct[j]<-sum(entry[j]>final)
  }
  ct  
}



#step 1: filter out all individuals with only a single policy
#potential future step: look at how many policies an individual has in force.  
#note: add filter by app type
ind <- rw  %>% group_by(fn,ln,ssn)  %>% filter(n()>1) %>% 
           mutate(num_prev_apps = rank(sum_entrydate,ties.method="min")-1 ) %>% 
           arrange(sum_entrydate) %>%  mutate(num_cur_apps = number_current_apps(sum_entrydate,final_date)) %>%
           mutate(prev_surv=cumsum(app_type=='SRV')) 

rw$rptd_date[rw$app_type %in% c('SRV','CNV','OPT')]=as.POSIXct('9999-12-31')


ind2<- rw %>% group_by(fn,ln,ssn)  %>% filter(n()>1) %>% arrange(sum_entrydate) %>% 
            mutate(cur_pol=number_current_policies(sum_entrydate,rptd_date))

indfull<-merge(ind,ind2[,names(ind2) %in% c("polnum","cur_pol")],all.x = TRUE)
                                                             
ind<-indfull[,names(indfull) %in% c('cur_pol','prev_surv','num_cur_apps','num_prev_apps','polnum')]
save(ind,file='PR_Ind.Rda')

           
agt <- rw  %>% group_by(sum_agentcode) %>%  arrange(sum_entrydate) %>%
              mutate(agt_length_of_service=difftime(sum_entrydate,min(sum_entrydate),units="weeks")) 
agt<-agt[,names(agt) %in% c('agt_length_of_service','polnum')]

save(agt,file='PR_agt.Rda')

agcy <- rw  %>% group_by(sum_agency_code) %>%  arrange(sum_entrydate) %>%
  mutate(agcy_length_of_service=difftime(sum_entrydate,min(sum_entrydate),units="weeks"))

agcy<-agcy[,names(agcy) %in% c('agcy_length_of_service','polnum')]
save(agcy,file='PR_agcy.Rda')
