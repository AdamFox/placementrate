
from pyspark import SparkContext

from pyspark.conf import SparkConf

conf = SparkConf()

conf.set('spark.local.dir', '/hadoop/sparklocal')

sc =SparkContext(conf=conf)

import json
import lxml.etree as etree
from datetime import datetime
from collections import Counter
import time

infile = '/data/ezapp/mssql/103s/' + time.strftime("%Y-%m") + '.jsonlines'
outcsv = 'data/ezapp_update.csv'

class ezapp:
	def __init__(self, line):                   # Whenever a line of json files are feeded
		x = json.loads(line)                    # It will automatically load it, and extra
		self.policy = x['POL_NBR']              # This is an attribute of the ezapp class.
		x = x['PAYLOAD']                        #
		self.root = etree.fromstring(x)         # This is the main body (root) of the class?

	def getpath(self, path):
		return self.root.xpath(path, namespaces = {"a": "http://ACORD.org/Standards/Life/2"})

	######################################################
	def TransExeDate(self):
		x = self.getpath("//a:TransExeDate")
		app_date = datetime.strptime(x[0].text, '%Y-%m-%d')
		return app_date


	def issueAgeOfPrimaryInsured(self):
		x = self.getpath("//a:Holding[@id='Application_Holding']/a:Policy/a:Life/a:Coverage/a:LifeParticipant[@PartyID='Party_PrimaryInsured']/a:IssueAge")
		age = [s.text for s in x]
		if not age:
			x = self.getpath("//a:Holding[@id='Additional_Holding']/a:Policy/a:Life/a:Coverage/a:LifeParticipant[@PartyID='Party_PrimaryInsured']/a:IssueAge")
			age = [s.text for s in x]
		if not age:
			x = self.getpath("//a:Holding[@id='Alternate_Holding']/a:Policy/a:Life/a:Coverage/a:LifeParticipant[@PartyID='Party_PrimaryInsured']/a:IssueAge")
			age = [s.text for s in x]
		if not age:
			age = ['NA']
		return age


	def tostring(self):
		return etree.tostring(self.root, pretty_print = True)
       #####################################################
        def holding(self):
                x = self.getpath("//a:Holding")
                return [s.attrib['id'] for s in x]

        def lineBusiness(self):
                x = self.getpath("//a:Holding[@id='Application_Holding']/a:Policy/a:LineOfBusiness")
                return [s.text for s in x]

        def face(self):
                x = self.getpath("//a:Holding[@id='Application_Holding']/a:Policy/a:Life/a:FaceAmt")
                return [s.text for s in x]

        def baseCoverage(self):
                x = self.getpath("//a:Holding[@id='Application_Holding']/a:Policy/a:Life/a:Coverage[a:IndicatorCode='Base']/a:PlanName")
                return [s.text for s in x]
                
        def riders(self):
                x = self.getpath("//a:Holding[@id='Application_Holding']/a:Policy/a:Life/a:Coverage[a:IndicatorCode='Rider']/a:PlanName")
                return [s.text for s in x]

        def estSalary(self):
                x = self.getpath("//a:Party[@id='Party_PrimaryInsured']/a:Person/a:EstSalary")
                return [s.text for s in x]

        def estIncome(self):
                x = self.getpath("//a:Household/a:EstIncome")
                return [s.text for s in x]

        def estNetwork(self):
                x = self.getpath("//a:Party[@id='Party_PrimaryInsured']/a:EstNetWorth")
                return [s.text for s in x]

        def estGrossAnnualOtherIncome(self):
                x = self.getpath("//a:Party[@id='Party_PrimaryInsured']/a:Person/a:EstGrossAnnualOtherIncome")
                return [s.text for s in x]

        def riskclass(self):
                x = self.getpath("//a:FormInstance[@id='FORM_A1AGE']/a:FormResponse[@id='FR_1245']/a:ResponseText")
                return [s.text for s in x]

        def otherInsApp(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1000']/a:FormResponse[@id='FR_1160']/a:ResponseData")
				return [s.text for s in x]

        def totalInforce(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1000']/a:FormResponse[@id='FR_1158']/a:ResponseText")
				return [s.text for s in x]

        def PurpBus(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1000']/a:FormResponse[@id='FR_1132']/a:ResponseText")
				return [s.text for s in x]

        def PurpPers(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1000']/a:FormResponse[@id='FR_1131']/a:ResponseText")
				return [s.text for s in x]

        def incentives(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1000']/a:FormResponse[@id='FR_1135']/a:ResponseData")
				return [s.text for s in x]

        def IntFin(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1AGE']/a:FormResponse[@id='FR_1257']/a:ResponseData")
				return [s.text for s in x]

        def Offer(self):
				x = self.getpath("//a:FormInstance[@id='FORM_A1AGE']/a:FormResponse[@id='FR_1246']/a:ResponseData")
				return [s.text for s in x]


lines = sc.textFile(infile, 100)

lines_all = lines.map(lambda x: [ezapp(x).TransExeDate(),
        ezapp(x).policy,
        ezapp(x).face(),
        ezapp(x).riskclass(),
        ezapp(x).estSalary(),
        ezapp(x).estGrossAnnualOtherIncome(),
        ezapp(x).estIncome(),
        ezapp(x).estNetwork(),
        ezapp(x).otherInsApp(),
        ezapp(x).totalInforce(),
        ezapp(x).Offer(),
        ezapp(x).IntFin(),
        ezapp(x).incentives(),
        ezapp(x).PurpPers(),
        ezapp(x).PurpBus(),
        ])

# Collect all the result, and write CSV
# Ideally, you should do it in a parallel way, but it's fine
result = lines_all.collect()

with open(outcsv, 'w') as f1:
        f1.write('AppDate, PolicyNumber, face, risk, estSalary, ' +
                'estGrossAnnualOtherIncome, estIncome, estNetworth, ' + 
                'OtherApps, TotalInForce, Offer, IntFinance, incentives, '+
                 'PurposePersonal, PurposeBusiness'+ '\n')
        for row in result:
                f1.write(str(row[0]) + ', ' +  str(row[1]) + ', ' +                     # year and policy #
                        str(row[2]) + ', ' + str(row[3]) + ', ' +  str(row[4]) + ', ' +         # APP
                        str(row[5])+ ', ' +  str(row[6])+ ', ' +  str(row[7]) + ', ' +
                        str(row[8])+ ', ' +  str(row[9])+ ', ' +  str(row[10])+ ',' +
                        str(row[11]) + ', ' +  str(row[12])+ ', ' +  str(row[13])+ ', ' +  str(row[14]) + '\n') 