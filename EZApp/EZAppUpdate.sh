export SPARK_HOME=/opt/cloudera/parcels/CDH/lib/spark/
export HADOOP_CONF_DIR=/etc/hadoop/conf 
export PYSPARK_PYTHON=/hadoop/virt/2.7/bin/python
export PYTHONPATH=$SPARK_HOME/python
export PATH="/hadoop/spark/1.6/bin:${PATH}"

spark-submit --master yarn --executor-cores 1 --queue randomqueue EZAppData_dailyupdate.py

echo -e "EZ-app data extracted.\n" && \

# 1. /hadoop/teradatar/teradatar.R is an argument for the settings of Teradata.

Rscript EZAppVertica.R