suppressPackageStartupMessages(library(RJDBC)) # this may stop the R session and ask you to install Java runtime, go ahead and do this
con <- JDBC(driverClass = "com.vertica.jdbc.Driver", 
            classPath = "/opt/vertica/java/lib/vertica-jdbc-7.1.1-0.jar")

# By this way, these elements will be re-usable again.
vertica_username = Sys.getenv('dbUser')
vertica_password = Sys.getenv('dbPass')
vertica_server = 'vertica.private.massmutual.com'
vertica_database = 'advana'
vertica_vsql = '/usr/local/bin/vsql'

db <- dbConnect(drv = con,
                "jdbc:vertica://vertica.private.massmutual.com:5433/advana", 
                vertica_username, 
                vertica_password)
message('Vertica admin profiles and RJDBC loaded. Connection named "db".')