# install necessary R packages into this project's directories
# to avoid permissions issues with installing to the default /usr/lib folder

path=r_packages
declare -a packages=("RJDBC","gbm","reshape2","PerformanceAnalytics","quantmod","lubridate","plyr","dplyr","readxl","jsonlite")

for p in "${packages[@]}"
do
	echo "installing $p"
	Rscript -e "install.packages('$p', repos = 'http://cran.us.r-project.org', lib = '$path')"

	# test to make sure they were installed
	# must add current directory to .libPaths so code knows where to find packages
	echo "testing $p was installed correctly"
	Rscript -e ".libPaths(c('$path', .libPaths())); library($p);"
done