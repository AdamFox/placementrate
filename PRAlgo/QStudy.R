s<-filter(s,sum_entrydate>as.POSIXct('2015-03-01'))
s<-filter(s,!is.na(s$old))

mon<-c(3:12,1,2)
e<-c()
a<-c()
t<-c()
nnn<-c()
o<-c()
for(i in mon){
  m<-filter(s,month(final_date)==i )#& wprem >= 33000 )
  e<-c(e,sum(m$wprem*m$preds))
  a<-c(a,sum(m$target*m$wprem))
  o<-c(o,sum(m$old*m$wprem))
  t<-c(t,sum(m$wprem))
  nnn<-c(nnn,sum(m$wprem>33000)/nrow(m))
  }

ea<-a/t - e/t
cea<-cumsum(a)/cumsum(t)-cumsum(e)/cumsum(t)

eao<-a/t - o/t
ceao<-cumsum(a)/cumsum(t)-cumsum(o)/cumsum(t)

#ea<- e/a
#cea<-cumsum(e)/cumsum(a)

library(ggplot2)
plt<-data.frame(timeframe=c(rep("MTD",12),rep("YTD",12)),val=100*c(ea,cea),month=c(1:12,1:12))
ggplot(data=plt,aes(x=month,y=val,color=timeframe)) + 
  geom_line(size=1.25) +
  geom_hline(yintercept=0,color="black",linetype='dashed') + 
  scale_color_manual(values=c("blue","red")) +
  xlab('') +
  ylab('% Reported, Actual vs Expected') +
  theme_bw() +
  scale_y_continuous(limits = c(-10, 10)) +
  scale_x_continuous(breaks = seq(1,12,by=2),labels=c("3/15",  "5/15","7/15","9/15","11/15","1/16") )+
  theme(axis.text.x= element_text(size=16, angle=30)) +
  theme(axis.title.y = element_text(size = 24)) +
  theme(axis.text.y= element_text(size=16))   +
  theme(legend.text=element_text(size=16)) 
#########################################################################
d<-read.csv("../TrainingFiles/Existing WL Placement Algorithm.csv")
Test$Old<-d$Score[match(Test$polnum,d$policy_no)]
Test<-Test[!is.na(Test$Old),]
Test<-filter(Test,wprem>=33000)
totrep=sum(Test$amount*Test$target)
results<-data.frame(perc=seq(1,100,by=1),old=rep(0,100),perf=rep(0,100),gbm=rep(0,100),face=rep(0,100))

#Sort by face value
face<-order(Test$amount,decreasing=TRUE)
old<-order(Test$Old*Test$amount,decreasing=TRUE)
perf<-order(Test$target*Test$amount,decreasing=TRUE)
gbm<-order(Test$preds*Test$amount,decreasing=TRUE)

for(k in seq(1,100,by=1)){
  results$old[round(1*k)]=sum(Test$amount[old[1:round(nrow(Test)*k/100)]] * Test$target[old[1:round(nrow(Test)*k/100)]] ) / totrep
  results$face[round(1*k)]=sum(Test$amount[face[1:round(nrow(Test)*k/100)]] * Test$target[face[1:round(nrow(Test)*k/100)]] ) /totrep
  results$perf[round(1*k)]=sum(Test$amount[perf[1:round(nrow(Test)*k/100)]] * Test$target[perf[1:round(nrow(Test)*k/100)]] ) /totrep
  results$gbm[round(1*k)]=sum(Test$amount[gbm[1:round(nrow(Test)*k/100)]] * Test$target[gbm[1:round(nrow(Test)*k/100)]] ) /totrep
}
results$old=results$old/results$face
results$gbm=results$gbm/results$face
results$perf=results$perf/results$face
results$face=results$face/results$face

library(reshape2)
melted=melt(T[,names(T) %in% c('perc','old','perf','gbm')],id.vars='perc')

ggplot(data=melted,aes(x=perc,y=value,group=variable,color=factor(variable,labels=c('Existing','Perfect','Phase 1'))  )) + 
  geom_line(size=1.25) + 
  geom_hline(yintercept=1,color="black",linetype='dashed') +
  scale_color_manual(values=c("blue","red","#009E73")) +
  labs(color="Model") +
  xlab('Percent of Top Applications Evaluated') +
  ylab('Change to Total Reported Face Amount') +
  theme_bw() +
  theme(axis.text.x= element_text(size=16)) +
  theme(axis.title.y = element_text(size = 16)) +
  theme(axis.title.x = element_text(size = 16)) +
  theme(axis.text.y= element_text(size=16))   +
  theme(legend.text=element_text(size=16)) 

require(caTools)
AUC_old=trapz(results$perc,results$old)/100
AUC_face=trapz(results$perc,results$face)/100
AUC_perf=trapz(results$perc,results$perf)/100
AUC_gbm=trapz(results$perc,results$gbm)/100
print( (AUC_gbm-AUC_face) / (AUC_perf-AUC_face)   )

####################################################
T<-results
T[,2:5]<-T[,2:5]*totrep/1000000
T$face=T$face-T$old
T$gbm=T$gbm-T$old-T$face
T$perf=T$perf-T$old-T$face-T$gbm



T<-filter(T,perc %in% c(10,25,50,75))
T$perc<-c(1,2,3,4)
T<-T[,c(1,5,4,3)]
melted=melt(T[,names(T) %in% c('perc','perf','gbm','face')],id.vars='perc')
ggplot(data=melted,aes(x=perc,y=value,fill=variable )) + 
  geom_bar(stat='identity',aes(position='stack') ) +
  scale_fill_manual(values=c("cyan4","red3","grey72"),
                    name="",
                    breaks=c("perf", "gbm", "face"),
                    labels=c("Perfect", "Pre-Approval", "Face")) +
  xlab('Percent of Top Applications Evaluated') +
  ylab('$ Benefit (m)') +
  theme_bw() +
  theme(legend.title=element_blank() )+
  theme(axis.text.x= element_text(size=16)) +
  theme(axis.title.y = element_text(size = 16)) +
  theme(axis.title.x = element_text(size = 16)) +
  theme(axis.text.y= element_text(size=16))   +
  theme(legend.text=element_text(size=16)) +
  scale_x_continuous(breaks = 1:4,labels=c("10",  "25","50","75") )
  


