WITH dates AS (
 select
    policy_no,
    min(case when value2 in ('DECL','INCP','NTRT','RPTD','DROP','WDRN','NTKN') then date_occured else cast('9999-12-31' as date) end) as final_date,
    min(CASE WHEN value2 = 'APVD' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS apvd_date
from
	winrisk.summary
	left join winrisk.Policy_Events on sum_polnum=policy_no
	left join winrisk.name on name_polnum=sum_polnum
where 
	name_role = 1
    and sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2014-01-01' as date) and  sum_entrydate < cast('2016-07-31' as date)
group by
    policy_no
)

select
    a.name_polnum as polnum,
    a.name_firstname as fn,
    a.name_lastname as ln,
    a.name_idnumber as ssn,
    a.name_sex as sex,
    a.name_age as age,
    a.name_marital as marital,
    a.name_tobacco_rating as tob_rating,
    b.amount,
    b.cov_code,
    d.sum_entrydate,
    e.exp_premium,
    CASE
        WHEN e.premium_mode in ('MA','MS','M','SM','TM','5','TT','7','MQ') then 12*e.prod_premium
        WHEN e.premium_mode in('3','Q','SQ') then 4*e.prod_premium
        WHEN e.premium_mode in ('2','S') then 2*e.prod_premium
        ELSE e.prod_premium
    END as annual_prem,
    CASE
        WHEN d.sum_entrydate between cast('2009-09-14' as date) and cast('2009-10-16' as date)
        OR d.sum_entrydate between cast('2010-09-13' as date) and cast('2010-10-15' as date)
        OR d.sum_entrydate between cast('2011-09-12' as date) and cast('2011-10-14' as date)
        OR d.sum_entrydate between cast('2012-09-10' as date) and cast('2012-10-12' as date)
        OR d.sum_entrydate between cast('2013-09-09' as date) and cast('2013-10-11' as date)
        OR d.sum_entrydate between cast('2014-09-08' as date) and cast('2014-10-10' as date)
        OR d.sum_entrydate between cast('2015-09-14' as date) and cast('2015-10-16' as date)
        OR d.sum_entrydate between cast('2016-09-12' as date) and cast('2016-10-14' as date)
        THEN 1
        ELSE 0
    END as qb,
    CASE
        WHEN d.sum_team in ('G','L') THEN 1
        ELSE 0
    END as brk,
    d.sum_status as status,
    d.sum_app_type as app_type,
    e.amount_1035,
    e.cash_with_app,
    e.dividend_mode,
    f.num_riders,
    f.total_rider_amt,
    f.WP,
    f.ALIR,
    CASE WHEN g.replacement_code IN ('C','N','X') THEN 1 ELSE 0 END as intRepl,
    CASE WHEN g.replacement_code IN ('C','F','M') THEN 1 ELSE 0 END as extRepl,
    h.final_date,
    h.apvd_date,
    
    CASE
        WHEN a.name_age > 70 OR b.amount>5000000 THEN 2
        WHEN a.name_age BETWEEN 61 AND 70 THEN 1
        WHEN b.amount > 500000 AND a.name_age<=16 THEN 1
        WHEN b.amount > 2000000 AND a.name_age>=51 THEN 1
        WHEN b.amount > 3000000 AND a.name_age>=41 THEN 1 
        ELSE 0
    END as aps_strt,
    k.num_ratings,
    n.num_und_reqs,
    n.num_iss_reqs,
    n.FieldAPS,
    n.paramed_ex,
    n.mv_report,
    n.ecg_test,
    n.blood_profile,
    r.xtra_und_reqs
    
from
    winrisk.Name a
LEFT JOIN    dates h on h.policy_no=a.name_polnum
LEFT JOIN    winrisk.summary d on a.name_polnum = d.sum_polnum
LEFT JOIN    winrisk.coverages b on a.name_polnum = b.policy_no
LEFT JOIN    winrisk.Billing e on a.name_polnum = e.policy_no
LEFT JOIN    winrisk.replacement_summary g on a.name_polnum = g.policy_no

LEFT JOIN    (select policy_no, count(*) as num_ratings 
                from winrisk.ratings group by policy_no ) k on a.name_polnum = k.policy_no
                
LEFT JOIN (
select count(*) as num_riders,
       sum(amount) as total_rider_amt,
       sum(case when rider_code = 'WP' then 1 else 0 end) as WP,
       sum(case when rider_code = 'ALIR' then 1 else 0 end) as ALIR,
       policy_no 
from
    winrisk.riders
group by
        policy_no) f on a.name_polnum=f.policy_no
          
LEFT JOIN    
(select dt.policy_no, 		
		sum(CASE WHEN requirement_id='FAPS' then 1 else 0 end) as FieldAPS,
		sum(CASE WHEN requirement_id='EXM' then 1 else 0 end) as paramed_ex,
		sum(CASE WHEN requirement_id='MVRP' then 1 else 0 end) as mv_report,
		sum(CASE WHEN requirement_id='ECG' then 1 else 0 end) as ecg_test,
		sum(CASE WHEN requirement_id='BLPR' then 1 else 0 end) as blood_profile,
        sum(CASE WHEN type_code='A' then 1 else 0 end) as num_und_reqs,
        sum(CASE WHEN type_code='I' then 1 else 0 end) as num_iss_reqs
                from dates dt  
                left join  winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and type_code in ('I', 'A') and order_date<apvd_date and order_date < final_date
                group by dt.policy_no) n on a.name_polnum = n.policy_no
	
	
LEFT JOIN(
	select dt.policy_no,
		   CASE WHEN max(order_date) > min(last_status_date) THEN 1 ELSE 0 END as xtra_und_reqs
	from dates dt
	left join winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and type_code='A' and order_date<apvd_date and order_date < final_date
	group by dt.policy_no ) r on r.policy_no= a.name_polnum
                
where
    a.name_role = 1
    and b.cov_code != 'ECTL'
    and d.sum_status NOT IN ('99CA','99UR','APVD','BMAI','BPEN','MAIL','RMAI','RPEN')
    and d.sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2014-01-01' as date) and  sum_entrydate < cast('2016-07-31' as date);