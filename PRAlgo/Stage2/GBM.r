library(dplyr)
library(gbm)
library(pROC)
library(lubridate)
library(lift)

setwd("~/Documents/Projects/placementrate/PRAlgo")
source("Stage2/DataPreProcS2.R")

d <- read.csv('~/Desktop/Data/PR_phase2_Jan2014TESTcode.csv')
d$sum_entrydate<-as.POSIXct(d$sum_entrydate)
d$apvd_date <-as.POSIXct(d$apvd_date)
d$rptd_date <- as.POSIXct(d$rptd_date)

d<- datapreproc(d)

# Train/Test
Train<-d[d$sum_entrydate<as.POSIXct('2015-06-01'),]
Test<-d[d$sum_entrydate>=as.POSIXct('2015-06-01'),]
set.seed(32)
s<-sample(nrow(Test),round(0.5*nrow(Test)))
Train<-rbind(Train,Test[s,])
Test <- Test[-s,] 

# filter out any policies not approved
Test<- filter(Test, year(apvd_date) < 9000)
Train <- filter(Train, year(apvd_date) <9000)
rm(d)

notfts<-c("replacement_code",
          "X",
          "rptd_date",
          'sum_entrydate',
          'fn',
          'ln',
          'ssn',
          'status',
          'polnum',
          'final_date', 
          'apvd_date', 
          'num_xclusions', 'face', 'risk', 'non_medical',
          # add all variables below 1 imporance
          'val1325',
          'wprem',
          'risk_class_change',
          'num_tests',
          'num_prev_apps',
          'PurposePersonal',
          'app_type',
          'marital',
          'estGrossAnnualOtherIncome',
          'OtherApps',
          'sex',
          'brk',
          'cur_pol',
          'Offer',
          'PurposeBusiness',
          'has_term',
          'prev_surv',
          'prev_term_failed',
          'exp_premium', 'total_num_reqs')


model=gbm(target~., data=Train[,!(names(Train) %in% notfts)], n.trees=15000, interaction.depth=15, cv.folds=5, shrinkage = .005)
save(file="~/Desktop/Data/finalPR_stage2_gbm.Rda", model)
