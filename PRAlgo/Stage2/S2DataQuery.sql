WITH dates AS (
 select
    policy_no,
    MIN(CASE WHEN value2 = 'MAIL' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS apvd_date,
    MIN(CASE WHEN value2 = 'RPTD' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS rptd_date,
    min(case when value2 in ('DECL','INCP','NTRT','RPTD','DROP','WDRN','NTKN') then date_occured 
    else cast('9999-12-31' as date) end) as final_date
from
winrisk.summary
left join winrisk.Policy_Events on sum_polnum=policy_no
left join winrisk.name on name_polnum=sum_polnum
where 
name_role = 1
    and sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2014-01-01' as date) AND sum_entrydate < cast('2016-07-31' as date)
group by
    policy_no
)

select
    a.name_polnum as polnum,
    a.name_firstname as fn,
    a.name_lastname as ln,
    a.name_idnumber as ssn,
    a.name_sex as sex,
    a.name_age as age,
    a.name_marital as marital,
    a.name_tobacco_rating as tob_rating,
    b.amount,
    b.cov_code,
    d.sum_entrydate,
    e.exp_premium,
    CASE
        WHEN e.premium_mode in ('MA','MS','M','SM','TM','5','TT','7','MQ') then 12*e.prod_premium
        WHEN e.premium_mode in('3','Q','SQ') then 4*e.prod_premium
        WHEN e.premium_mode in ('2','S') then 2*e.prod_premium
        ELSE e.prod_premium
    END as annual_prem,
    CASE
        WHEN d.sum_team in ('G','L') THEN 1
        ELSE 0
    END as brk,
    d.sum_status as status,
    d.sum_app_type as app_type,
    e.cash_with_app,
    f.total_rider_amt,
    g.replacement_code,
    h.final_date,
    h.rptd_date,
    h.apvd_date,
    n.paramed_ex,
    n.num_und_reqs,
    n.num_iss_reqs,
    p.num_notes,
    r.xtra_und_reqs
    
from
    winrisk.Name a
    
LEFT JOIN    dates h on h.policy_no=a.name_polnum
LEFT JOIN    winrisk.summary d on a.name_polnum = d.sum_polnum
LEFT JOIN    winrisk.coverages b on a.name_polnum = b.policy_no
LEFT JOIN    winrisk.Billing e on a.name_polnum = e.policy_no
LEFT JOIN    winrisk.replacement_summary g on a.name_polnum = g.policy_no

                
LEFT JOIN
(select policy_no,
       sum(amount) as total_rider_amt
        from
            winrisk.riders
        group by
                policy_no) f on a.name_polnum=f.policy_no
          
LEFT JOIN    
(select nt.policy_no,  
        sum(CASE WHEN nt.note_date < dt.apvd_date then 1 else 0 end) as num_notes
        from winrisk.notes nt 
                left join dates dt on nt.policy_no=dt.policy_no
                group by nt.policy_no 
                ) p on a.name_polnum = p.policy_no 
                
                
LEFT JOIN    
(select rq.policy_no, 
        sum(CASE WHEN status_code = 'R' AND requirement_id='EXM' then 1 else 0 end) as paramed_ex,
        sum(CASE WHEN type_code='A' then 1 else 0 end) as num_und_reqs,
        sum(CASE WHEN status_code = 'R' AND type_code='I' then 1 else 0 end) as num_iss_reqs
               from winrisk.requirements rq  
                left join dates dt on dt.policy_no=rq.policy_no
                where insured_id=1
                group by rq.policy_no) n on a.name_polnum = n.policy_no

    
LEFT JOIN
(select dt.policy_no,
    CASE WHEN max(order_date) > min(last_status_date) THEN 1 ELSE 0 END as xtra_und_reqs
        from dates dt
        left join winrisk.requirements rq on dt.policy_no=rq.policy_no
            where insured_id=1 and type_code='A' and order_date < apvd_date
            group by dt.policy_no ) r on r.policy_no= a.name_polnum    
    
                
where
    a.name_role = 1
    and cov_code != 'ECTL'
    and sum_status NOT IN ('99CA','99UR','APVD','BMAI','BPEN','MAIL','RMAI','RPEN')
    and d.sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2014-01-01' as date) AND sum_entrydate < cast('2016-07-31' as date);