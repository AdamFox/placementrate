WITH dates AS (
 select
    policy_no,
    min(case when value2 in ('DECL','INCP','NTRT','RPTD','DROP','WDRN','NTKN') then date_occured else cast('9999-12-31' as date) end) as final_date,
    min(CASE WHEN value2 = 'APVD' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS apvd_date
from
	winrisk.summary
	left join winrisk.Policy_Events on sum_polnum=policy_no
	left join winrisk.name on name_polnum=sum_polnum
where 
	name_role = 1
    and sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2014-09-01' as date) and  sum_entrydate < cast('2016-07-31' as date)
group by
    policy_no
)

select
    a.name_polnum as polnum,
    d.sum_entrydate,
    h.final_date,
    h.apvd_date,
    n.max_can_und_reqs,
    n.min_can_und_reqs,
	q.max_late_reqs,
	q.min_late_reqs
    
from
    winrisk.Name a
LEFT JOIN    dates h on h.policy_no=a.name_polnum
LEFT JOIN    winrisk.summary d on a.name_polnum = d.sum_polnum
                
                
LEFT JOIN    
(select dt.policy_no,
		   MIN(Last_status_date) as min_can_und_reqs,
		   MAX(Last_status_date) as max_can_und_reqs
	from dates dt
	left join winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and status_code = 'D' and type_code = 'A' and order_date<apvd_date and order_date < final_date
                group by dt.policy_no) n on a.name_polnum = n.policy_no

LEFT JOIN(
	select dt.policy_no,
			MIN(due_date)  as min_late_reqs,
		   MAX(due_date) as max_late_reqs
	from dates dt
	left join winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and type_code in ('I', 'A') 
                and order_date<apvd_date and order_date < final_date
                and datediff(d,Last_status_date,due_date) < 0
	group by dt.policy_no ) q on q.policy_no= a.name_polnum
	            
where
    a.name_role = 1
    and d.sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2014-09-01' as date) and  sum_entrydate < cast('2016-07-31' as date);
