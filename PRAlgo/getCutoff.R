thres<-seq(0,1,by=0.005)

eact<-c()
for(i in 1:length(thres)){
  eact<-c(eact,sum(ifelse(T$preds>thres[i],1,0) )/sum(T$target))
}


bct<-thres[which( abs(eact-1) == min(abs(eact-1)) )]


eact<-c()
for(i in 1:length(thres)){
  eact<-c(eact,sum(ifelse(T$preds>thres[i],T$wprem,0) )/sum(T$target*T$wprem))
}


bap<-thres[which( abs(eact-1) == min(abs(eact-1)) )]


sum(ifelse(T$preds>bct,1,0) )/sum(T$target)
sum(ifelse(T$preds>bap,1,0) )/sum(T$target)
sum(ifelse(T$preds>mean(c(bct,bap)),1,0) )/sum(T$target)


sum(ifelse(T$preds>bct,T$wprem,0) )/sum(T$target*T$wprem)
sum(ifelse(T$preds>bap,T$wprem,0) )/sum(T$target*T$wprem)
sum(ifelse(T$preds>mean(c(bct,bap)),T$wprem,0) )/sum(T$target*T$wprem)

