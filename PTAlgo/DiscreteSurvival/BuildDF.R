#d<-read.csv('../PTTrain.csv')
#d$sum_entrydate<-as.POSIXct(d$sum_entrydate)
#d$apvd_date<-as.POSIXct(d$apvd_date)
#d$rptd_date<-as.POSIXct(d$rptd_date)
#d$last_status_date<-as.POSIXct(d$last_status_date)
#d$order_date<-as.POSIXct(d$order_date)

#save(file='/Users/mm99518/Documents/Placement.Rate/PTDatesTran.Rda',d)
load(file='/Users/mm99518/Documents/Placement.Rate/PTDatesTran.Rda')

#target
d$target<-ceiling(as.numeric(difftime(d$rptd_date,d$sum_entrydate,units="weeks")))
d$apvd<-ceiling(as.numeric(difftime(d$apvd_date,d$sum_entrydate,units="weeks")))
numwk<-unique(d[,names(d) %in% c('polnum','target','tob_rating','amount','cov_code',
                                 'annual_prem','amount_1035','cash_with_app','num_riders',
                                 'intRepl','extRepl','apvd','app_type','age','sum_entrydate','qb')] )
library(dplyr)
library(lubridate)
#create training dataframe of policy numbers.
pt<- data.frame(polnum=rep(numwk$polnum,numwk$target),risk=rep(numwk$tob_rating,numwk$target),
                face=rep(numwk$amount,numwk$target), cov_code=rep(numwk$cov_code,numwk$target),
                age=rep(numwk$age,numwk$target), month=rep(month(numwk$sum_entrydate),numwk$target),
                aprem=rep(numwk$annual_prem,numwk$target), a1035=rep(numwk$amount_1035,numwk$target),
                cwa=rep(numwk$cash_with_app,numwk$target), num_riders=rep(numwk$num_riders,numwk$target),
                intRepl=rep(numwk$intRepl,numwk$target), extRepl=rep(numwk$extRepl,numwk$target),
                apvd=rep(numwk$apvd,numwk$target),target=rep(numwk$target,numwk$target),
                qb=rep(numwk$qb,numwk$target),app_type=rep(numwk$app_type,numwk$target),
                OpenUndReqs=rep(0,sum(numwk$target)),ClosedReqs=rep(0,sum(numwk$target)),
                OpenIssReqs=rep(0,sum(numwk$target)),PostIssReqs=rep(0,sum(numwk$target)),
                MiscReqs=rep(0,sum(numwk$target)))


#add week numbers
pt<- pt %>% group_by(polnum) %>% mutate (week = row_number()) %>% ungroup()

#fix apvd, target
pt$apvd<-ifelse(pt$apvd > (.2 + pt$week),0,1)
pt$target<-ifelse(pt$target < (pt$week+1.5) ,1,0)

#add reqs
rq<-d[,names(d) %in% c('polnum','order_date','last_status_date','sum_entrydate','type_code')]
rq$order_date<-ceiling(as.numeric(difftime(d$order_date,d$sum_entrydate,units="weeks")))
rq$last_status_date<-ceiling(as.numeric(difftime(d$last_status_date,d$sum_entrydate,units="weeks")))
rq$order_date[rq$order_date==0]=1
rq$sum_entrydate<-NULL
rq$rptd_date<-NULL

rq<- rq %>% group_by(polnum,order_date,last_status_date,type_code) %>% summarise(n=n())

mapreqs<-function(j)  {
  part<-seq(1,nrow(rq),length.out=8)
  part<-round(part)
  for(i in part[j]:(-1+part[j+1])){
    pt$ClosedReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$last_status_date[i]] <<- pt$ClosedReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$last_status_date[i]] + rq$n[i]
    if(rq[i,4]=='A'){
      pt$OpenUndReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i] & pt$week< rq$last_status_date[i]] <<-  pt$OpenUndReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i] & pt$week< rq$last_status_date[i]] + rq$n[i]
    } else if(rq[i,4]=='I'){
      pt$OpenIssReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i] & pt$week< rq$last_status_date[i]] <<-  pt$OpenIssReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i] & pt$week< rq$last_status_date[i]] + rq$n[i] 
    } else if(rq[i,4]=='R'){
      pt$PostIssReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i]] <<-  pt$PostIssReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i]] + rq$n[i] 
    } else if(rq[i,4]=='N'){
      pt$MiscReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i] ] <<- pt$MiscReqs[pt$polnum == rq$polnum[i] & pt$week>= rq$order_date[i]] + rq$n[i]}
  }
  file=sprintf('../savepart%i.Rda',j)
  save(file=file,pt)
  return(1)
}


library(doMC)
library(foreach)
registerDoMC(cores=7)
junk<-foreach(i=1:7) %dopar% mapreqs(i)


#load("~/Documents/Placement.Rate/savepart1.Rda")
#pt1=pt
# load("~/Documents/Placement.Rate/savepart2.Rda")
# pt2=pt
# load("~/Documents/Placement.Rate/savepart3.Rda")
# pt3=pt
# load("~/Documents/Placement.Rate/savepart4.Rda")
# pt4=pt
# load("~/Documents/Placement.Rate/savepart5.Rda")
# pt5=pt
# load("~/Documents/Placement.Rate/savepart6.Rda")
# pt6=pt
# load("~/Documents/Placement.Rate/savepart7.Rda")
# pt7=pt
 
# pt$numOpenReqs=pt1$numOpenReqs+pt2$numOpenReqs+pt3$numOpenReqs+pt4$numOpenReqs+pt5$numOpenReqs+pt6$numOpenReqs+pt7$numOpenReqs
# pt$numClosedReqs=pt1$numClosedReqs+pt2$numClosedReqs+pt3$numClosedReqs+pt4$numClosedReqs+pt5$numClosedReqs+pt6$numClosedReqs+pt7$numClosedReqs
 
# t<-filter(pt,polnum==15684465)
# t[1:10,]
 