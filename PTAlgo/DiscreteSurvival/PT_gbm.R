library(gbm)
library(pROC)
library(lubridate)
library(dplyr)
source('PTPreProc.R')

pt<-PTPreProc()
notfts<-c('polnum','OpenPostIssReqs','OpenMiscReqs')

load('../TestPolNum.Rda')
Test<-pt[pt$polnum %in% TestPolNum,]
Train<-pt[!(pt$polnum %in% TestPolNum),]

model=gbm(target~., data = Train[,!(names(Train) %in% notfts)], verbose = TRUE, n.trees=10000,interaction.depth=10,cv.folds = 5)


#preds=predict(model,newdata = Train[1:250000,],n.trees=gbm.perf(model),type='response')
#print(auc(Train$target[1:250000],preds))
save(file="~/documents/placement.rate/PT_Run3.Rda",model)
