WITH dates AS (
SELECT 
    policy_no,
    MIN(CASE WHEN value2 = 'MAIL' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS apvd_date,
    MIN(CASE WHEN value2 = 'RPTD' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS rptd_date    
FROM
    winrisk.Policy_Events 
WHERE value2 IN ('MAIL', 'RPTD')
GROUP BY 
    policy_no
)


SELECT
	dt.apvd_date,
	dt.rptd_date,
	n.name_polnum AS polnum,
    s.sum_app_type AS app_type,
    s.sum_entrydate AS entry_date,
    s.sum_status AS status,
    c.cov_code AS cov_code,
    c.amount AS amount,
    b.cash_with_app,
    b.dividend_mode,
    b.exp_premium AS exp_premium,
    b.amount_1035 AS amount_1035,
    n.name_tobacco_rating AS tob_rating,
    n.name_sex AS sex,
    n.name_age AS age,
    n.name_marital AS marital,
    p.num_notes,
    n1.num_und_reqs,
	n1.num_iss_reqs,
	n1.FieldAPS,
    k.num_ratings,
    m.num_xclusions,
    CASE
        WHEN REGEXP_LIKE(name_street1, '#') THEN 1 
        WHEN REGEXP_LIKE(name_street1, 'APT') THEN 1
        Else 0 
    END as in_apt,
    CASE
        WHEN b.premium_mode in ('MA','MS','M','SM','TM','5','TT','7','MQ') then 12*b.prod_premium
        WHEN b.premium_mode in('3','Q','SQ') then 4*b.prod_premium
        WHEN b.premium_mode in ('2','S') then 2*b.prod_premium
        ELSE b.prod_premium
    END as annual_prem,
    CASE
        WHEN s.sum_entrydate between cast('2009-09-14' as date) and cast('2009-10-16' as date)
        OR s.sum_entrydate between cast('2010-09-13' as date) and cast('2010-10-15' as date)
        OR s.sum_entrydate between cast('2011-09-12' as date) and cast('2011-10-14' as date)
        OR s.sum_entrydate between cast('2012-09-10' as date) and cast('2012-10-12' as date)
        OR s.sum_entrydate between cast('2013-09-09' as date) and cast('2013-10-11' as date)
        OR s.sum_entrydate between cast('2014-09-08' as date) and cast('2014-10-10' as date)
        OR s.sum_entrydate between cast('2015-09-14' as date) and cast('2015-10-16' as date)
        OR s.sum_entrydate between cast('2016-09-12' as date) and cast('2016-10-14' as date)
        THEN 1
        ELSE 0
    END as qb,
    CASE
        WHEN c.amount<50000 THEN 1
        ELSE 0
    END as small_pol,
    CASE
        WHEN n.name_age < 17 THEN 1
        ELSE 0
    END as minor,
    CASE
        WHEN s.sum_team in ('G','L') THEN 1
        ELSE 0
    END as brk,
    g.replacement_code,
    q.num_late_reqs,
    r.xtra_und_reqs,
    CASE
        WHEN n.name_age > 70 OR c.amount>5000000 THEN 2
        WHEN n.name_age BETWEEN 61 AND 70 THEN 1
        WHEN c.amount > 500000 AND n.name_age<=16 THEN 1
        WHEN c.amount > 2000000 AND n.name_age>=51 THEN 1
        WHEN c.amount > 3000000 AND n.name_age>=41 THEN 1 
        ELSE 0
    END as aps_strt
    				
      
FROM
    winrisk.name as n 
    
LEFT JOIN    dates AS dt ON dt.policy_no = n.name_polnum
LEFT JOIN    winrisk.summary AS s ON n.name_polnum = s.sum_polnum
LEFT JOIN    winrisk.coverages AS c ON n.name_polnum = c.policy_no
LEFT JOIN    winrisk.Billing AS b ON n.name_polnum = b.policy_no
LEFT JOIN    winrisk.replacement_summary g on n.name_polnum = g.policy_no
LEFT JOIN    (select policy_no, count(*) as num_ratings 
                from winrisk.ratings group by policy_no ) k on n.name_polnum = k.policy_no
LEFT JOIN    (select policy_no, count(*) as num_xclusions 
                from winrisk.exclusion group by policy_no ) m on n.name_polnum = m.policy_no  

LEFT JOIN(
	select dt.policy_no,
		   SUM(CASE WHEN datediff(d,Last_status_date,due_date) < 0 THEN 1 ELSE 0 END) as num_late_reqs
	from dates dt
	left join winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and type_code in ('I', 'A') and order_date<apvd_date
	group by dt.policy_no ) q on q.policy_no= n.name_polnum

LEFT JOIN    
(SELECT nt.policy_no,  
        SUM(CASE WHEN nt.note_date < dt.apvd_date THEN 1 ELSE 0 END) AS num_notes 
                FROM winrisk.notes nt 
                LEFT JOIN dates dt ON nt.policy_no=dt.policy_no
                GROUP BY nt.policy_no
                ) p ON n.name_polnum = p.policy_no 

LEFT JOIN    
(select dt.policy_no, 		
		sum(CASE WHEN requirement_id='FAPS' then 1 else 0 end) as FieldAPS,
        sum(CASE WHEN type_code='A' then 1 else 0 end) as num_und_reqs,
        sum(CASE WHEN type_code='I' then 1 else 0 end) as num_iss_reqs
                from dates dt  
                left join  winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and type_code in ('I', 'A') and order_date<apvd_date
                group by dt.policy_no) n1 on n.name_polnum = n1.policy_no
               
LEFT JOIN(
	select dt.policy_no,
		   CASE WHEN max(order_date) > min(last_status_date) THEN 1 ELSE 0 END as xtra_und_reqs
	from dates dt
	left join winrisk.requirements rq on dt.policy_no=rq.policy_no
                where insured_id=1 and type_code='A' and order_date<apvd_date
	group by dt.policy_no ) r on r.policy_no= n.name_polnum
	
	
WHERE
    n.name_role = 1
    AND s.sum_app_type NOT IN ('SRV','TMP','INC')
    AND s.sum_entrydate >= CAST('2014-01-01' AS DATE) AND s.sum_entrydate < CAST('2015-07-01' AS DATE);