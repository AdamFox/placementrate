WITH dates AS (
 select
    policy_no,
    min(case when value2 in ('DECL','INCP','NTRT','RPTD','DROP','WDRN','NTKN') then date_occured else cast('9999-12-31' as date) end) as final_date,
    min(CASE WHEN value2 = 'APVD' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS apvd_date,
    min(CASE WHEN value2 = 'MAIL' THEN date_occured ELSE CAST('9999-12-31' AS date) END) AS issue_date
    
from
	winrisk.summary
	left join winrisk.Policy_Events on sum_polnum=policy_no
	left join winrisk.name on name_polnum=sum_polnum
where 
	name_role = 1
    and sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2013-01-01' as date) and  sum_entrydate < cast('2016-01-31' as date)
group by
    policy_no
)

select
	a.name_age as age,
	b.amount,
    b.cov_code,
    d.sum_entrydate as entry_date,
	d.sum_app_type as app_type,
	d.sum_status as status,
	e.exp_premium,
    e.amount_1035,
    h.final_date,
    h.apvd_date,
    h.issue_date
  from
    winrisk.Name a
    
LEFT JOIN    dates h on h.policy_no = a.name_polnum
LEFT JOIN    winrisk.summary d on a.name_polnum = d.sum_polnum
LEFT JOIN    winrisk.coverages b on a.name_polnum = b.policy_no
LEFT JOIN    winrisk.Billing e on a.name_polnum = e.policy_no        
where
    a.name_role = 1
    and b.cov_code != 'ECTL'
    and d.sum_status NOT IN ('99CA','99UR','APVD','BMAI','BPEN','MAIL','RMAI','RPEN')
    and d.sum_app_type NOT IN ('SRV','TMP','INC')
    and sum_entrydate >= cast('2013-01-01' as date) and  sum_entrydate < cast('2016-01-31' as date);